import React from "react";
import { useHistory } from 'react-router-dom';
import {
  FooterContainer,
  GroupContainer,
  Text,
  SocialMediaGroup,
  SocialMediaTitle,
  SocialMediaLogos,
  TextConfidence,
  ContainerBrand,
  BrandContainer,
  TextBrand,
  Logo,
  ImgAnchor,
  ContainerLogo,
  PrivacyPolicyContainer,
  PrivacyPolicyAnchor,
} from "./styles";
import LogoRal from "../../assets/images/logos/logowhite.ral.png";
import InstragramRal from "../../assets/images/logos/logo.instagram.white.png";
import LinkedinRal from "../../assets/images/logos/logo.in.white.png";
import FacebookRal from "../../assets/images/logos/logo.facebook.white.png";

export const Footer = ({ color, id }) => {
  const history = useHistory();

  function handleRedirect(dir = '/') {
    history.push(dir);
  }

  return (
    <FooterContainer id={id} color={color}>
      <GroupContainer>
        <ContainerBrand>
          <ContainerLogo>
            <Logo
              onClick={() => handleRedirect('/')}
              src={LogoRal}
              alt='Logo de RAL'
            />
          </ContainerLogo>
          <Text>
            Constructora RAL de Occidente, S.A de C.V.<br />
            Torre Hipodromo, Av. patria 1721, Interior 9,<br />
            Colomos Providencia, Guadalajara.<br />
          </Text>
        </ContainerBrand>
        <SocialMediaGroup>
          <SocialMediaTitle>Síguenos</SocialMediaTitle>
          <SocialMediaLogos>
            <ImgAnchor href="https://www.linkedin.com/company/constructora-ral-de-occidente/">
              <Logo src={LinkedinRal} alt="linkedin ral" />
            </ImgAnchor>
            <ImgAnchor href="https://www.facebook.com/ConstructoraRal">
              <Logo src={FacebookRal} alt="facebook ral" />
            </ImgAnchor>
            <ImgAnchor href="https://www.instagram.com/constructora_ral/">
              <Logo src={InstragramRal} alt="instsgram ral" />
            </ImgAnchor>
          </SocialMediaLogos>
        </SocialMediaGroup>
        <TextConfidence>
          Construimos Confianza
        </TextConfidence>
      </GroupContainer>
      <BrandContainer>
        <TextBrand>2022 Constructora RAL de Occidente, S.A de C.V. Todos los derechos reservados.</TextBrand>
        <TextBrand>Make with love <b>Malmo.</b></TextBrand>
      </BrandContainer>
      <PrivacyPolicyContainer>
        <PrivacyPolicyAnchor>Politica de Privacidad</PrivacyPolicyAnchor>
      </PrivacyPolicyContainer>
    </FooterContainer>
  );
};
