import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";
import { blue, white } from "../../assets/styles/colors";

export const ButtonStyled = styled.a`
  font-family: "Conv_Adelle_Bold", Sans-Serif;
  background-color: ${(props) => (props.disabled ? "#DADADA" : blue)};
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  padding: 12px 25px 8px;
  box-shadow: 0 10px 14px rgba(0, 0, 0, 0.2);
  border-radius: 15px;
  font-weight: bold;
  font-size: 24px;
  color: ${(props) => (props.color !== "primary" ? white : blue)};
  margin: 10px;
  margin-left: ${(props) => props.marginLeft};
  cursor: pointer;
  transition: all 0.25s ease;
  &:active {
    transform: scale(0.95);
  }
  @media ${deviceDF.laptopL} {
    font-size: 22px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 21px;
  }
  @media ${deviceDF.laptopS} {
    font-size: 20px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 19px;
  }
  @media ${deviceDF.tabletM} {
    font-size: 17px;
  }
  @media ${deviceDF.mobileL} {
    text-align: center;
  }
`;
