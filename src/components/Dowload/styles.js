import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";
import { black, blue } from "../../assets/styles/colors";

export const Container = styled.div`
  position: relative;
  width: 85%;
  display: flex;
  background-color: #ebedff;
  border-radius: 50px;
  justify-content: center;
  margin-bottom: 90px;
  @media ${deviceDF.mobileL} {
    width: 95%;
  }
`;

export const SectionIcon = styled.div`
  position: absolute;
  left: 0;
  bottom: -65px;
  left: -5vw;
  @media ${deviceDF.tabletM} {    
    bottom: -45px;
    left: -5vw;
  }
  @media ${deviceDF.tabletS} {
    bottom: -40px;
  }
  @media ${deviceDF.mobileL} {
    bottom: -35px;
    left: -5vw;
  }
`;

export const Icon = styled.img`
  height: 180px;
  width: 200px;
  @media ${deviceDF.tabletM} {
    height: 140px;
    width: 150px;
  }
  @media ${deviceDF.tabletS} {
    height: 120px;
    width: 130px;
  }
  @media ${deviceDF.mobileL} {
    height: 120px;
    width: 130px;
  }
`;

export const Button = styled.a`
  position: absolute;
  background-color: ${blue};
  right: 0px;
  bottom: 0px;
  top: 0px;
  border-style: none;
  border-radius: 50px;
  color: white;
  width: 35%;
  font-size: 23px;
  font-family: "Conv_Adelle_Reg";
  @media ${deviceDF.tabletL} {
    font-size: 19px;
    width: 32%;
  }
  @media ${deviceDF.tabletM} {
    font-size: 19px;
  }
  @media ${deviceDF.mobileL} {
    width: 30%;
  }
  @media ${deviceDF.mobileM} {
  }
`;

export const TextBtn = styled.p`
  margin: 15px auto;
  text-align: center;
  font-size: 25px;
  font-family: "Conv_Adelle_Reg";
  @media ${deviceDF.tabletL} {
    margin: 20px auto;
    font-size: 20px;
  }
  @media ${deviceDF.tabletM} {
    font-size: 17px;
  }
  @media ${deviceDF.tabletS} {
    margin: 15px auto;
    font-size: 15px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 12px;
  }
  @media ${deviceDF.mobileS} {
    margin: 13px auto;
    font-size: 12px;
  }
`;

export const DowloadText = styled.p`
  font-family: "Conv_Adelle_Bold";
  font-size: 20px;
  margin: 17px;
  margin-left: -5vw;
  width: 40%;
  color: ${black};
  @media ${deviceDF.laptopM} {
    margin-left: -10vw;
  }
  @media ${deviceDF.tabletL} {
    padding: 3px;
    font-size: 18px;
    width: 45%;
    margin-left: -8vw;
  }
  @media ${deviceDF.tabletM} {
    font-size: 16px;
    width: 50%;
    margin-left: -10vw;
  }
  @media ${deviceDF.tabletS} {
    font-size: 14px;
    margin: 12px;
    width: 55%;
    margin-left: -6vw;
  }
  @media ${deviceDF.mobileL} {
    text-align: center;
    margin: 12px;
    width: 50%;
    font-size: 12px;
    margin-left: -7vw;
  }
  @media ${deviceDF.mobileS} {
    font-size: 10px;
  }
`;
