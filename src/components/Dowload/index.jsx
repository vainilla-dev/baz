import * as React from 'react'
import { Container, Icon, SectionIcon, Button, TextBtn, DowloadText } from "./styles"
import BarraProyectos from "../../assets/images/barraproyectos.ral.png"
import PDF from "../../assets/images/pdfs/cv.ral.pdf"

export const Dowload = () => {

  return (
    <Container>
      <SectionIcon>
        <Icon src={BarraProyectos} />
      </SectionIcon>
      <DowloadText>
        Conoce mas de nuestros proyectos
      </DowloadText>
      <Button href={PDF} target="_blank">
        <TextBtn>Descargar curriculum</TextBtn>
      </Button>
    </Container >
  )
}