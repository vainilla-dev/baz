import * as React from "react"
import { Card } from './styles';

export const CardContainer = ({ children }) => {
  return (
    <Card>
      {children}
    </Card>
  )
}