import styled from "styled-components";
import { blue } from "../../assets/styles/colors";
import { deviceDF } from "../../assets/styles/breakPoints";
export const Card = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: column;
  padding: 32px;
  margin: 30px;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.05), 0 0px 40px rgba(0, 0, 0, 0.08);
  border-radius: 24px;
  background-color: ${blue};
  width: 400px;
  transition: all 300ms ease;
  &:hover {
    transform: translateY(-30px);
  }
  @media ${deviceDF.mobileL} {
    width: auto;
  }
  @media ${deviceDF.mobileM} {
    width: auto;
  }
  @media ${deviceDF.laptopM} {
    width: auto;
  }
`;
