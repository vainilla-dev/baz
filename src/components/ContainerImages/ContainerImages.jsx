import React from 'react'
import { ImgBuilingComponent , ContainerImg,} from "./styles"

export function ContainerImages({ img, alt, onShow }) {
  return (
    <ContainerImg>
      <ImgBuilingComponent src={img} alt={ alt} onClick={() => onShow('', true)} />
    </ContainerImg>
  )
}
