import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";
import { blue } from "../../assets/styles/colors";

export const NavbarContainer = styled.div`
  width: 100%;
  height: 60px;
  background-color: transparent;
  padding: 0 3rem 0 2.2rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media ${deviceDF.laptopS} {
    padding: 0 1rem;
    justify-content: center;
  }
`;

export const NavLogo = styled.div`
  display: flex;
  flex: 50%;
  align-items: center;
  margin: 0.5rem;
  @media ${deviceDF.mobileM} {
    width: 70px;
  }
  @media ${deviceDF.mobileS} {
    width: 70px;
  }
`;

export const NavContact = styled.div`
  display: flex;
  flex: 20%;
  align-items: center;
  justify-content: space-around;
  margin: 0.5rem;
  @media ${deviceDF.mobileM} {
    display: none;
  }
  @media ${deviceDF.mobileS} {
    display: none;
  }
  @media ${deviceDF.laptopS} {
    display: none;
  }
  @media ${deviceDF.tabletM} {
    margin: 0.4rem;
  }
  @media ${deviceDF.tabletL} {
    margin: 0.4rem;
  }
`;

export const NavLink = styled.a`
  font-family: "Conv_Adelle_Semi_Bold";
  font-size: 20px;
  color: ${blue};

  @media ${deviceDF.laptopL} {
    font-size: 16px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 16px;
  }
  @media ${deviceDF.laptopS} {
    font-size: 12px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 8px;
  }
  @media ${deviceDF.tabletM} {
    font-size: 8px;
  }
`;

export const Logo = styled.img`
  width: 60px;
  height: auto;
  cursor: pointer;
  @media ${deviceDF.tabletM} {
    width: 70px;
  }
  @media ${deviceDF.mobileM} {
    width: 70px;
  }
  @media ${deviceDF.mobileS} {
    width: 70px;
  }
`;
