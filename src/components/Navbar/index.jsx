import React from 'react';
import { useHistory } from 'react-router-dom';
import { NavbarContainer, Logo, NavLink, NavContact, NavLogo } from './styles';
import LogoRal from '../../assets/images/logos/logo.ral.png';
import { Button } from '../../components/Button';

export const Navbar = () => {
  const history = useHistory();
  
  function handleRedirect(dir = '/') {
    history.push(dir);
  }

  return (
    <>
      <NavbarContainer>
        <NavLogo>
          <Logo
            onClick={() => handleRedirect('/')}
            src={LogoRal}
            alt='Logo de RAL' />
        </NavLogo>
        <NavContact>
          <NavLink onClick={() => handleRedirect('/acerca-de-nosotros')}>{`Quienes somos >`}</NavLink>
          <NavLink onClick={() => handleRedirect('/proyectos')}>{`Proyectos >`}</NavLink>
          <Button href='#footer'>Contacto</Button>
        </NavContact>
      </NavbarContainer>
    </>
  );
};
