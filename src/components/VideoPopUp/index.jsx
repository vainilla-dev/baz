import React from "react";
import CancelIcon from "../../assets/images/icons/cancel-yellow.png";
import { VideoContainer, CloseContainer, Icon } from "./styles.js";
import ReactPlayer from "react-player";

export const VideoPopUp = ({ close }) => {
  return (
    <>
      <CloseContainer>
        <Icon onClick={close} src={CancelIcon} />
      </CloseContainer>
      <VideoContainer>
        <VideoContainer>
          <ReactPlayer
            config={{
              vimeo: {
                playerOptions: {
                  responsive: true,
                },
              },
            }}
            width="100%"
            height="100%"
            url="https://vimeo.com/576857174"
            playing={true}
            muted={true}
            // controls={false} // For Vimeo videos, hiding controls must be enabled by the video owner. (Search 'controls' props at https://www.npmjs.com/package/react-player)
            loop={true}
          />
        </VideoContainer>
      </VideoContainer>
    </>
  );
};
