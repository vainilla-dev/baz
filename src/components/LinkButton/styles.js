import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";
import { white, blue } from "../../assets/styles/colors";

export const AnchorStyled = styled.a`
  grid-column: ${(props) => props.FullWidth && "1 / 3"};
  font-family: "Conv_Adelle_Bold", Sans-Serif;
  background-color: ${blue};
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  padding: 12px 15px 8px;
  box-shadow: 0 10px 14px rgba(0, 0, 0, 0.2);
  border-radius: 50px;
  font-weight: bold;
  text-decoration: none;
  text-align: center;
  font-size: 24px;
  color: ${white};

  margin: 10px;
  margin-left: ${(props) => props.marginLeft};

  cursor: pointer;
  width: 300px;
  @media ${deviceDF.laptopL} {
    font-size: 20px;
  }
  @media ${deviceDF.tabletM} {
  }
  @media ${deviceDF.mobileL} {
  }
`;
