import React from "react";
import { Button } from "../Button"
import { ButtonsContainer } from './styles'

export const ButtonsContact = ({ children, ...props }) => {
  return (
    <>
      <ButtonsContainer {...props}>
        <Button {...props}>{`${children}`}</Button>
      </ButtonsContainer>
    </>
  );
};
