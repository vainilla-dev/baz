import React from "react";
import { HashRouter, Switch, Route } from "react-router-dom";
import { GlobalStyle } from "../../assets/styles/GlobalStyles";
import Popup from "reactjs-popup";
import { blue } from "../../assets/styles/colors";
import { Home } from "../../views/Home";
import { Projects } from "../../views/Projects";
import { AboutUs } from "../../views/AboutUs";
import { Layout } from "../../components/Layout";
import ScrollToTop from "../../components/ScrollToTop";
import { BurgerIcon } from "../BurgerIcon";
import { BurgerMenu } from "../BurgerMenu";

const contentStyle = {
  background: blue,
  width: "80%",
  border: "none",
};

const App = () => {
  return (
    <>
      <GlobalStyle />
      <HashRouter>
        <ScrollToTop>
          <Switch>
            <Route exact path="/" component={Home} />
            <Layout>
              <Route exact path="/acerca-de-nosotros" component={AboutUs} />
              <Route exact path="/proyectos" component={Projects} />
            </Layout>
            <Route component={Home} />
          </Switch>
        </ScrollToTop>
        <Popup
          modal
          overlayStyle={{ background: blue }}
          contentStyle={contentStyle}
          closeOnDocumentClick={false}
          trigger={(open) => <BurgerIcon open={open} />}
        >
          {(close) => <BurgerMenu close={close} />}
        </Popup>
      </HashRouter>
    </>
  );
};

export default App;
