import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";

export const Background = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: rgba(0, 0, 0, 0.85);
  display: flex;
  padding: 4rem 6rem;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  z-index: ${(props) => (props.show ? "-1" : " 5001")};
`;

export const ButtonCloseWrapper = styled.div`
  z-index: 5002;
  position: absolute;
  top: 15px;
  right: 15px;
  color: #fff;
  cursor: pointer;
  transition: all 0.25s ease;
  &:active {
    transform: scale(0.9);
  }
`;

export const Img = styled.div`
  width: 70%;
  display: flex;
  justify-content: center;
  @media ${deviceDF.tabletL} {
    width: 55%;
  }
  @media ${deviceDF.tabletM} {
    width: 75%;
  }
  @media ${deviceDF.mobileL} {
    width: 110%;
  }
  @media ${deviceDF.mobileM} {
    width: 130%;
  }
  @media ${deviceDF.mobileS} {
    width: 140%;
  }
`;

