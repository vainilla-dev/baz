import React from "react";
import ReactDOM from "react-dom";
import { XCircle } from "react-feather";
import {
  Background,
  ButtonCloseWrapper,
  Img,
} from "./styles";

export const GlobalBlur = ({ isOpen = false, onClose = () => { }, children }) => {
  if (!isOpen) {
    return null;
  } else {
    return ReactDOM.createPortal(
      <Background>
        <ButtonCloseWrapper onClick={onClose}>
          <XCircle size={32} />
        </ButtonCloseWrapper>
        <Img>{children}</Img>
      </Background>,
      document.getElementById("blur")
    );
  }
};
