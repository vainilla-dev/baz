import styled from "styled-components";
import { black } from "../../assets/styles/colors";
import { deviceDF } from "../../assets/styles/breakPoints";

export const MainSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 400px;
  overflow-x: hidden;
  @media ${deviceDF.tabletM} {
    align-items: center;
    height: 500px;
    margin: auto;
  }
  @media ${deviceDF.mobileL} {
    height: 650px;
    align-items: center;
    margin: auto;
    width: auto;
  }
  @media ${deviceDF.mobileM} {
    height: 650px;
    align-items: center;
    margin: auto;
    width: auto;
  }
`;

export const ContactTitle = styled.div`
  color: ${black};
  font-family: "Conv_Adelle_Bold";
  font-size: 38px;
  margin: 40px;
  @media ${deviceDF.tabletM} {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media ${deviceDF.mobileL} {
    font-size: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media ${deviceDF.mobileM} {
    font-size: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const SectionButtonContact = styled.div`
  display: flex;
  height: 200px;
  align-items: center;
  @media ${deviceDF.tabletM} {
    width: 100%;
    flex-direction: column;
  }
  @media ${deviceDF.mobileL} {
    width: 100%;
    flex-direction: column;
  }
  @media ${deviceDF.mobileM} {
    width: 100%;
    flex-direction: column;
  }
`;

export const ImgBuilingComponent = styled.img`
  width: 100%;
  height: auto;
  padding: 10px;
`;

export const SectionFormulario = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 300px;
  width: 100%;
  @media ${deviceDF.tabletM} {
    height: 400px;
    flex-direction:column ;
  }
`;

export const Modal = styled.div`
  width: 100%;
  height: auto;
  background-color: #ffffff;
  border-radius: 30px;
`;

export const ModalContent = styled.div`
  padding: 10px;
  align-items: center;
  font-family: "Conv_Adelle_Reg";
`;

export const Row = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 300px;
  grid-gap: 20px;
  transition: all 1s;
  width: 100%;
  height: 700px;
  padding: 0 60px;
  @media ${deviceDF.tabletM} {
    display: flex;
    flex-direction: column;
    align-items: center;
    height: auto;
  }
  @media ${deviceDF.mobileL} {
    height: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  @media ${deviceDF.mobileM} {
    height: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
