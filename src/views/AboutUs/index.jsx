import React, { useRef } from 'react';
import { useHistory } from 'react-router-dom';
import {
  SectionTitle,
  VideoSectionTitle,
  ExperienceSection,
  ExperienceGrid,
  Item,
  ItemData,
  ItemIcon,
  MapContainer,
  CoverageSection,
  ButtonsContainer,
  TextCoverage,
  MapImg,
  BoxMap,
  PlayerContainer,
  ContainerTitle,
  ContainerBtnContact
} from './styles';
import { Footer } from '../../components/Footer';
import { blue } from '../../assets/styles/colors';
import MapMexicoImage from '../../assets/images/mexico.ral.png';
import Place from '../../assets/images/place.ral.png';
import Publicos from '../../assets/images/clientes-publicos.png';
import PublicosSS from '../../assets/images/clientes-publicos-small-screens.png';
import Privados from '../../assets/images/clientes-privados.png';
import PrivadosSS from '../../assets/images/clientes-privados-small-screens.png';
import { ButtonsContact } from '../../components/ButtonsContact';
import { ClientButtonsContact } from '../../components/ButtonsContact/clientButtonsContact';
import { Dowload } from '../../components/Dowload';
import { useCountUp } from 'react-countup';
import { GlobalBlur } from '../../components/GlobalBlur/GlobalBlur'
import Certificados from '../../assets/images/certificaciones.png'
import { NavbarContainer, Logo, NavLink, NavContact, NavLogo } from '../../components/Navbar/styles';
import LogoRal from '../../assets/images/logos/logo.ral.png';
import { Button } from '../../components/Button';
import Player from '../../components/Player/Player';

export const AboutUs = () => {
  const [show, setShow] = React.useState(false)
  
  const [imgState, setImgState] = React.useState(false)

  const handleShowImagen = name => {
    switch (name) {
      case 'publicsSS':
        setShow(true)
        setImgState(PublicosSS)
        break;
      case 'privatesSS':
        setShow(true)
        setImgState(PrivadosSS)
        break;
      case 'privates':
        setShow(true)
        setImgState(Privados)
        break;
      default:
        setShow(true)
        setImgState(Publicos)
        break;
    }
  }

  const { countUp, start } = useCountUp({
    start: 0,
    end: 150,
    delay: 3,
    duration: 5,
  });

  const { countUp: countNumber, start: startNumber } = useCountUp({
    start: 0,
    end: 40,
    delay: 3,
    duration: 5,
  });

  const { countUp: countNumber2, start: startNumber2 } = useCountUp({
    start: 0,
    end: 50,
    delay: 3,
    duration: 5,
  });
  
  const [isEnded, setIsEnded] = React.useState(false);

  const handleStart = () => {
    if (!isEnded) {
      start();
      startNumber();
      startNumber2();
      setIsEnded(true);
    }
  };

  const history = useHistory();
  function handleRedirect(dir = '/') {
    history.push(dir);
  };

  const divRef = useRef(null);
  
  const playerProps = { playing: true, url: 'https://vimeo.com/715332548' };

  return (
    <>
      <NavbarContainer>
        <NavLogo>
          <Logo
            onClick={() => handleRedirect('/')}
            src={LogoRal}
            alt='Logo de RAL' />
        </NavLogo>
        <NavContact>
          <NavLink onClick={() => handleRedirect('/acerca-de-nosotros')}>{`Quienes somos >`}</NavLink>
          <NavLink onClick={() => handleRedirect('/proyectos')}>{`Proyectos >`}</NavLink>
          <Button onClick={() => { divRef.current.scrollIntoView({ behavior: 'smooth' }); }}>Contacto</Button>
        </NavContact>
      </NavbarContainer>
      <ContainerTitle>
        <VideoSectionTitle>
          CONSTRUCTORA RAL DE OCCIDENTE
        </VideoSectionTitle>
      </ContainerTitle>
      <PlayerContainer>
        <Player {...playerProps} />
      </PlayerContainer>
      <ContainerBtnContact>
        <ButtonsContact href={Certificados} target='_blank'>Nuestras certificaciones </ButtonsContact>
      </ContainerBtnContact>
      <ExperienceSection onMouseEnter={handleStart} onMouseOver={handleStart} onMouseMove={handleStart}>
        <SectionTitle>
          Experiencia y solidez que garantizan resultados y generan confianza
        </SectionTitle>
        <ExperienceGrid >
          <ItemData>
            <ItemIcon>
              +{countUp}
            </ItemIcon>
            <Item>
              Proyectos realizados de Infraestructura Urbana,
              Edificacion en general y obra civil industrial.
            </Item>
          </ItemData>
          <ItemData>
            <ItemIcon>
              +{countNumber}
            </ItemIcon>
            <Item>
              Años trabajando con presencia en la mayoria de la republica mexicana.
            </Item>
          </ItemData>
          <ItemData>
            <ItemIcon>
              Confianza
            </ItemIcon>
            <Item>
              Es nuestra palabra clave, la cual nos
              caracteriza por generar tranquilidad y
              seguridad en los proyectos de
              construcción que han confiado
              en nuestras manos.
            </Item>
          </ItemData>
          <ItemData>
            <ItemIcon>
              +{countNumber2}
            </ItemIcon>
            <Item>
              Clientes en cumplimiento total
              de requerimientos en termino de calidad,
              tiempo y seguridad industrial.
            </Item>
          </ItemData>
        </ExperienceGrid>
      </ExperienceSection>
      <MapContainer>
        <CoverageSection>
          <TextCoverage>
            Clientes
          </TextCoverage>
        </CoverageSection>
        <ButtonsContainer>
          <ClientButtonsContact onClick={() => handleShowImagen('publics')}>Públicos</ClientButtonsContact>
          <ClientButtonsContact onClick={() => handleShowImagen('privates')}>Privados</ClientButtonsContact>
          <ClientButtonsContact smallScreen={true} onClick={() => handleShowImagen('publicsSS')}>Públicos</ClientButtonsContact>
          <ClientButtonsContact smallScreen={true} onClick={() => handleShowImagen('privatesSS')}>Privados</ClientButtonsContact>
        </ButtonsContainer>
        <MapImg IconPlace height='40' src={Place} alt='place ral' />
        <TextCoverage>
          Cobertura
        </TextCoverage>
        <BoxMap src={MapMexicoImage} alt='mexico ral' />
        <Dowload />
      </MapContainer>
      <section id='footer' ref={divRef}>
        <Footer id='footer' color={blue} />
      </section>
      <GlobalBlur isOpen={show} customers={true} onClose={() => setShow(false)}>
        {<img src={imgState} alt='Clientes' width='100%' height='auto' />}
      </GlobalBlur>
    </>
  );
};
