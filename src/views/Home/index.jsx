import React, { useRef } from 'react';
import { useHistory } from 'react-router-dom';
import SwiperCore, { Autoplay, Navigation, Pagination, A11y } from 'swiper';
import ReactPlayer from "react-player";
import {
  ContainerPlayer,
  AboutSection,
  CertifiedImgItems,
  AboutImgItems,
  AboutText,
  TextContainer,
  StrongText,
  StrongCardText,
  WorkingSection,
  WorkingText,
  CertifiedSection,
  CertifiedContainer,
  CertifiedTitle,
  WorkingCardsBox,
  CardLogo,
  CardText,
  StrongTextBold,
  AboutImgSection,
} from './styles';
import 'swiper/swiper-bundle.css';
import hipodromo1 from '../../assets/images/hipodromo1.jpeg';
import hipodromo2 from '../../assets/images/hipodromo2.jpeg';
import hipodromo3 from '../../assets/images/hipodromo3.jpeg';
import { blue } from '../../assets/styles/colors';
import { Footer } from '../../components/Footer';
import Certified1 from '../../assets/images/certificado1.png';
import Certified2 from '../../assets/images/certificado2.png';
import Edification from '../../assets/images/edificacion.ral.png';
import Obra from '../../assets/images/obra.ral.png';
import Infrastructure from '../../assets/images/infrectura.ral.png';
import { GlobalBlur } from '../../components/GlobalBlur/GlobalBlur'
import { CardContainer } from '../../components/Card';
import { Dowload } from '../../components/Dowload';
import { Badge } from '../../components/Badge';
import Player from '../../components/Player/Player';
import { NavbarContainer, Logo, NavLink, NavContact, NavLogo } from '../../components/Navbar/styles';
import LogoRal from '../../assets/images/logos/logo.ral.png';
import { Button } from '../../components/Button';

SwiperCore.use([Autoplay, Navigation, Pagination, A11y]);

export const Home = () => {
  const playerProps = { playing: true, url: "https://vimeo.com/715331456" };

  const [show, setShow] = React.useState(false);

  const history = useHistory();
  function handleRedirect(dir = '/') {
    history.push(dir);
  };

  const divRef = useRef(null);

  return (
    <>
      <NavbarContainer>
        <NavLogo>
          <Logo
            onClick={() => handleRedirect('/')}
            src={LogoRal}
            alt='Logo de RAL' />
        </NavLogo>
        <NavContact>
          <NavLink onClick={() => handleRedirect('/acerca-de-nosotros')}>{`Quienes somos >`}</NavLink>
          <NavLink onClick={() => handleRedirect('/proyectos')}>{`Proyectos >`}</NavLink>
          <Button onClick={() => { divRef.current.scrollIntoView({ behavior: 'smooth' }); }}>Contacto</Button>
        </NavContact>
      </NavbarContainer>
      <ContainerPlayer>
        <Player {...playerProps} />
        <Badge color='primary' onClick={() => setShow(true)}>Video de marca {'>'}</Badge>
      </ContainerPlayer>
      <AboutSection>
        <TextContainer>
          <AboutText>
            <StrongText>TE DAMOS LA BIENVENIDA A <StrongTextBold>CONSTRUCTORA RAL DE OCCIDENTE</StrongTextBold></StrongText>
          </AboutText>
          <AboutText>
            Empresa familiar orgullosamente jaliscience con mas de 40 años de experiencia,
            dedicada a la contrucción de obras civiles
            de diversas indole, tales como Infraestructura Urbana,
            Obra Industrial y Edificación y Estructura.
          </AboutText>
        </TextContainer>
        <AboutImgSection>
          <AboutImgItems src={hipodromo1} alt='Hipodromo 1' />
          <AboutImgItems src={hipodromo2} alt='Hipodromo 2' />
          <AboutImgItems src={hipodromo3} alt='Hipodromo 3' />
        </AboutImgSection>
      </AboutSection>
      <CertifiedSection>
        <CertifiedTitle>
          {`"EXPERIENCIA Y SOLIDEZ QUE GARANTIZAN 
              RESULTADOS Y GENERAN CONFIANZA"`}
        </CertifiedTitle>
        <CertifiedContainer>
          <CertifiedImgItems src={Certified1} alt='gif' />
          <CertifiedImgItems src={Certified2} alt='gif' />
          <AboutText certifiedSize >
            Estamos altamente comprometidos con
            el servicio que ofrecemos a nuestros clientes
            por lo cual contamos con certificaciones
            internacionales de calidad y seguridad industrial.
          </AboutText>
        </CertifiedContainer>
      </CertifiedSection>
      <WorkingSection>
        <WorkingText color={blue}>¿Que tipo de proyectos realizamos?</WorkingText>
        <WorkingCardsBox>
          <CardContainer>
            <CardLogo
              onClick={() => handleRedirect('/proyectos')}
              src={Edification}
              alt='logo-card' />
            <CardText onClick={() => handleRedirect('/proyectos')}>
              Infraestructura <StrongCardText>Urbana</StrongCardText>
            </CardText>
          </CardContainer>
          <CardContainer>
            <CardLogo
              onClick={() => handleRedirect('/proyectos')}
              src={Infrastructure}
              alt='logo-card' />
            <CardText onClick={() => handleRedirect('/proyectos')}>
              Edificacion  <StrongCardText>y Estructura</StrongCardText>
            </CardText>
          </CardContainer>
          <CardContainer>
            <CardLogo
              onClick={() => handleRedirect('/proyectos')}
              src={Obra}
              alt='logo-card' />
            <CardText onClick={() => handleRedirect('/proyectos')}>
              Obra <StrongCardText>Industrial</StrongCardText>
            </CardText>
          </CardContainer>
        </WorkingCardsBox>
        <Dowload />
      </WorkingSection>
      <section id='footer' ref={divRef}>
        <Footer id='footer' color={blue} />
      </section>
      <GlobalBlur isOpen={show} onClose={() => setShow(false)}>
        <ReactPlayer
          url="https://vimeo.com/715332548"
          width="100%"
          height="100%"
          playing
          muted
          controls={false}
          loop
        />
      </GlobalBlur>
    </>
  );
};
