import styled from "styled-components";
import { blue, white } from "../../assets/styles/colors";
import { deviceDF } from "../../assets/styles/breakPoints";

export const ContainerPlayer = styled.div`
  position: relative;
`;

export const AboutSection = styled.section`
  width: 100%;
  height: auto;
  padding: 15vh;
  background-color: ${white};
  display: flex;
  align-items: center;
  position: relative;
  overflow: hidden;
  @media ${deviceDF.tabletM} {
    padding: 30px;
    flex-direction: column;
  }
  @media ${deviceDF.tabletS} {
    padding: 20px;
  }
  @media ${deviceDF.mobileM} {
    padding: 10px;
  }
`;

export const CertifiedImgItems = styled.img`
  margin: 3px;
  height: 200px;
  @media ${deviceDF.LaptopS} {
    height: 180px;
  }
`;

export const AboutText = styled.p`
  font-family: "Conv_Adelle_Reg";
  font-size: ${(props) => (props.certifiedSize ? "26px" : "30px")};
  padding: 0 2rem;
  display: inline-block;
  margin: 1rem 0;
  /* text-align: justify; */
  @media ${deviceDF.laptopL} {
    font-size: 24px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 18px;
  }
  @media ${deviceDF.tabletM} {
    text-align: center;
    margin: 1.5rem 0;
    padding: 0 5px;
  }
  @media ${deviceDF.tabletS} {
    font-size: 26px;
  }
  @media ${deviceDF.mobileL} {
    font-size: ${(props) => (props.certifiedSize ? "22px" : "20px")};
    margin: 2rem 0;
    padding: 0;
    width: 100%;
  }
  @media ${deviceDF.mobileM} {
    font-size: ${(props) => (props.certifiedSize ? "22px" : "20px")};
    padding: 0.9rem;
    width: 100%;
  }
`;

export const TextContainer = styled.div`
  width: 50%;
  text-align: center;
  @media ${deviceDF.tabletL} {
  }
  @media ${deviceDF.tabletS} {
    width: auto;
  }
  @media ${deviceDF.mobileL} {
    width: 100%;
    font-size: 24px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 20px;
  }
`;

export const StrongText = styled.p`
  font-family: "Conv_Adelle_Semi_Bold";
  font-size: 36px;
  color: ${blue};
  @media ${deviceDF.laptopM} {
    font-size: 24px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 17px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 24px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 20px;
  }
  
  @media ${deviceDF.laptopM} {
    font-size: 28px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 20px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 24px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 20px;
  }
`;

export const StrongCardText = styled.p`
  font-family: "Conv_Adelle_Bold";
  @media ${deviceDF.laptopM} {
    font-size: 18px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 16px;
  }
`;

export const WorkingSection = styled.section`
  width: 100%;
  height: auto;
  padding: 15px 80px;
  background-color: ${white};
  display: flex;
  flex-direction: column;
  align-items: center;

  @media ${deviceDF.tabletL} {
    flex-direction: column;
    height: auto;
    padding: 50px;
    padding-top: 65px;
  }
  @media ${deviceDF.tabletM} {
    flex-direction: column;
    height: auto;
    padding: 50px;
    padding-top: 65px;
  }
  @media ${deviceDF.laptopS} {
    height: auto;
  }
  @media ${deviceDF.mobileL} {
    padding: 30px;
    padding-top: 65px;
  }
  @media ${deviceDF.mobileM} {
    padding: 2.4rem 0.9rem;
    padding-top: 65px;
  }
`;

export const WorkingText = styled.strong`
  font-family: "Conv_Adelle_Bold";
  font-size: 36px;
  margin: 3rem 0 2.5rem 0;
  align-items: center;
  color: ${(props) => (props.color ? props.color : white)};
  @media ${deviceDF.laptopM} {
    font-size: 24px;
    width: auto;
    margin: auto;
  }
  @media ${deviceDF.mobileL} {
    font-size: 19px;
    width: auto;
    margin: auto;
  }
  @media ${deviceDF.mobileM} {
    font-size: 19px;
    width: auto;
    margin: auto;
  }
  @media ${deviceDF.mobileS} {
    font-size: 17px;
    width: auto;
    margin: auto;
  }
`;

export const CertifiedSection = styled.section`
  width: 100%;
  padding: 60px;
  padding-bottom: 100px;
  background-color: #ebedff;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  @media ${deviceDF.tabletM} {
    flex-direction: column;
    height: 1200px;
    padding: 30px;
  }
  @media ${deviceDF.tabletS} {
    flex-direction: column;
    height: 1200px;
    padding: 30px;
  }
  @media ${deviceDF.mobileL} {
    padding: 13px;
  }
  @media ${deviceDF.mobileM} {
    padding: 13px;
  }
`;

export const CertifiedContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 80%;
  @media ${deviceDF.tabletM} {
    flex-direction: column;
    align-items: center;
  }
  @media ${deviceDF.mobileL} {
    align-items: center;
  }
  @media ${deviceDF.mobileM} {
    align-items: center;
  }
`;

export const CertifiedTitle = styled.strong`
  font-size: 30px;
  text-align: center;
  width: 100%;
  font-family: "Conv_Adelle_Bold";
  margin: 50px auto;
  color: ${blue};
  align-items: center;
  @media ${deviceDF.tabletM} {
    font-size: 30px;
    padding: 20px 30px;
    align-items: center;
    margin: 10px;
  }
  @media ${deviceDF.tabletS} {
    font-size: 30px;
    padding: 20px 30px;
    align-items: center;
    margin: 10px;
    width: auto;
  }
  @media ${deviceDF.mobileL} {
    font-size: 24px;
    padding: 15px 20px;
    align-items: center;
    margin: 10px;
    width: auto;
  }
  @media ${deviceDF.mobileM} {
    font-size: 22px;
    padding: 15px 20px;
    margin: 10px;
    width: auto;
  }
`;

export const WorkingCardsBox = styled.div`
  width: 100%;
  margin: 10px;
  padding: 0 60px 60px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media ${deviceDF.tabletM} {
    flex-direction: column;
    height: auto;
    padding: 50px;
  }
  @media ${deviceDF.mobileL} {
    flex-direction: column;
    padding: 24px;
    width: auto;
  }
  @media ${deviceDF.mobileM} {
    flex-direction: column;
    padding: 16px;
    width: auto;
  }
`;

export const CardLogo = styled.img`
  cursor: pointer;
  margin: 50px auto;
  width: 150px;
  height: auto;
  align-items: center;
  @media ${deviceDF.tabletM} {
    margin: 12px auto;
  }
  @media ${deviceDF.laptopM} {
    margin: 30px auto;
    width: 120px;
  }
  @media ${deviceDF.tabletL} {
    margin: 30px auto;
    width: 100px;
  }
`;

export const CardText = styled.p`
  cursor: pointer;
  font-family: "Conv_Adelle_Light_Ita";
  color: ${white};
  text-align: center;
  font-size: 28px;
  @media ${deviceDF.tabletM} {
    font-size: 18px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 18px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 16px;
  }
`;

export const StrongTextBold = styled.strong`
  font-family: "Conv_Adelle_Extra_Bold";
  font-size: 36px;
  color: ${blue};
  @media ${deviceDF.laptopM} {
    font-size: 28px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 20px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 24px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 20px;
  }
`;

export const AboutImgItems = styled.img`
  border-style: solid;
  border-color: ${blue};
  border-radius: 15px;
  margin: 10px;
  width: 30%;
  height: auto;
  object-fit: cover;
  @media ${deviceDF.laptopL} {
    width: 33%;
  }
  @media ${deviceDF.laptopS} {
    width: 38%;
  }
  @media ${deviceDF.tabletM} {
    width: 35%;
    height: 40vh;
  }
  @media ${deviceDF.tabletS} {
    width: 40%;
    height: 35vh;
  }
  @media ${deviceDF.mobileM} {
    width: 50%;
    height: 30vh;
  }
`;

export const AboutImgSection = styled.div`
  display: flex;
  flex-direction: row;
  z-index: 2;
  width: 50%;
  left: 50%;
  @media ${deviceDF.tabletM} {
    width: 100%;
    left: 0;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;
