import TorreMerak from "../../assets/images/merak.png";
import TorreIntervalo from "../../assets/images/intervalo.png";
import TorreAlcalde from "../../assets/images/alcalde960.png";
import TorreArama from "../../assets/images/amarabay.png";
import TorreCentralia from "../../assets/images/centralia.png";
import TorreVentura from "../../assets/images/ventura.png";
import TorreMiralto from "../../assets/images/miralto.png";

export function edificationButtonData() {
  return [
    {
      img: TorreMerak,
      alt: "TorreMerak",
      descriptions: [
        "Obra: Torre Merak Westower",
        "Cliente: ADC Promotores",
        "Gerencia: ADC Promotores",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreIntervalo,
      alt: "TorreIntervalo",
      descriptions: [
        "Obra: Torre Intervalo Espacios",
        "Cliente: Barragan + Moreno",
        "Gerencia: Coarma",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreAlcalde,
      alt: "TorreAlcalde",
      descriptions: [
        "Obra: Torre Alcalde 960",
        "Cliente: DIAVAL",
        "Gerencia: DIAVAL",
        "Ubicación: Guadalajara, Jalisco.",
      ]
    },
    {
      img: TorreArama,
      alt: "TorreArama",
      descriptions: [
        "Obra: Torre Arama Bay",
        "Cliente: ECP",
        "Gerencia: Direxion",
        "Ubicación: Bucerias, Nayarit.",
      ]
    },
    {
      img: TorreCentralia,
      alt: "TorreCentralia",
      descriptions: [
        "Obra: Torre Centralia",
        "Cliente: Construvida",
        "Gerencia: Construvida",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreVentura,
      alt: "TorreVentura",
      descriptions: [
        "Obra: Torre 2 Fase 3 Ventura Vertical District",
        "Cliente: Tierra y Armonía",
        "Gerencia: Grupo DEO",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreMiralto,
      alt: "TorreMiralto",
      descriptions: [
        "Obra: Torre Miralto Vista",
        "Cliente: Construvida",
        "Gerencia: Construvida",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
  ];
}
