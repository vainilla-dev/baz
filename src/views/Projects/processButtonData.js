import TorreMerak from "../../assets/images/merak.png";
import TorreIntervalo from "../../assets/images/intervalo.png";
import TorreAlcalde from "../../assets/images/alcalde960.png";
import TorreArama from "../../assets/images/amarabay.png";
import TorreCentralia from "../../assets/images/centralia.png";
import TorreVentura from "../../assets/images/ventura.png";
import RehabilitacionAeropuertoPaz from "../../assets/images/aeropuertopaz.png";
import RehabilitacionAeropuertoCancun from "../../assets/images/aeropuertocancun.png";
import PlantaAcerera from "../../assets/images/plantaacerera.png";
import TorreMiralto from "../../assets/images/miralto.png";

export function processButtonData() {
  return [
    {
      img: TorreMerak,
      alt: "TorreMerak",
      descriptions: [
        "Obra: Torre Merak Westower",
        "Cliente: ADC Promotores",
        "Gerencia: ADC Promotores",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreIntervalo,
      alt: "TorreIntervalo",
      descriptions: [
        "Obra: Torre Intervalo Espacios",
        "Cliente: Barragan + Moreno",
        "Gerencia: Coarma",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreAlcalde,
      alt: "TorreAlcalde",
      descriptions: [
        "Obra: Torre Alcalde 960",
        "Cliente: DIAVAL",
        "Gerencia: DIAVAL",
        "Ubicación: Guadalajara, Jalisco.",
      ]
    },
    {
      img: TorreArama,
      alt: "TorreArama",
      descriptions: [
        "Obra: Torre Arama Bay",
        "Cliente: ECP",
        "Gerencia: Direxion",
        "Ubicación: Bucerias, Nayarit.",
      ]
    },
    {
      img: TorreCentralia,
      alt: "TorreCentralia",
      descriptions: [
        "Obra: Torre Centralia",
        "Cliente: Construvida",
        "Gerencia: Construvida",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: TorreVentura,
      alt: "TorreVentura",
      descriptions: [
        "Obra: Torre 2 Fase 3 Ventura Vertical District",
        "Cliente: Tierra y Armonía",
        "Gerencia: Grupo DEO",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
    {
      img: RehabilitacionAeropuertoPaz,
      alt: "RehabilitacionAeropuertoPaz",
      descriptions: [
        "Obra: Rehabilitación de plataformas y rodajes en Aeropuerto",
        "Cliente: GAP",
        "Gerencia: GAP",
        "Ubicación: La Paz, BCS.",
      ]
    },
    {
      img: RehabilitacionAeropuertoCancun,
      alt: "RehabilitacionAeropuertoCancun",
      descriptions: [
        "Obra: Rehabilitación de rodajes en Aeropuerto",
        "Cliente: ASUR",
        "Gerencia: ASUR",
        "Ubicación: Cancún, Quintana Roo.",
      ]
    },
    {
      img: PlantaAcerera,
      alt: "PlantaAcerera",
      descriptions: [
        "Obra: Movimiento de tierras, demoliciones y cimentaciones en planta acerera",
        "Cliente: TERNIUM",
        "Gerencia: TERNIUM",
        "Ubicación: Monterrey, Nuevo Leon.",
      ]
    },
    {
      img: TorreMiralto,
      alt: "TorreMiralto",
      descriptions: [
        "Obra: Torre Miralto Vista",
        "Cliente: Construvida",
        "Gerencia: Construvida",
        "Ubicación: Zapopan, Jalisco.",
      ]
    },
  ];
}
