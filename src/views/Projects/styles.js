import styled from "styled-components";
import { black } from "../../assets/styles/colors";
import { deviceDF } from "../../assets/styles/breakPoints";

export const MainSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: auto;
  overflow-x: hidden;
  @media ${deviceDF.tabletM} {
    align-items: center;
    height: 550px;
    margin: auto;
  }
  @media ${deviceDF.tabletS} {
    height: 540px;
  }
  @media ${deviceDF.mobileL} {
    height: 530px;
  }
  @media ${deviceDF.mobileM} {
    height: 520px;
  }
  @media ${deviceDF.mobileS} {
    height: 500px;
  }
`;

export const ContactTitle = styled.div`
  color: ${black};
  font-family: "Conv_Adelle_Bold";
  font-size: 38px;
  margin: 40px;
  @media ${deviceDF.tabletM} {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media ${deviceDF.mobileL} {
    font-size: 25px;
    text-align: center;
  }
`;

export const SectionButtonContact = styled.div`
  display: flex;
  height: auto;
  align-items: center;
  justify-content: space-between;
  @media ${deviceDF.tabletM} {
    width: auto;
    flex-direction: column;
  }
  @media ${deviceDF.mobileL} {
    width: 100%;
  }
`;

export const ImgBuilingComponent = styled.img`
  width: 100%;
  height: auto;
  padding: 10px;
  object-fit: cover;
`;

export const SectionFormulario = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5vh;
  height: auto;
  width: 100%;
  @media ${deviceDF.tabletM} {
    height: 400px;
    flex-direction: column;
  }
`;

export const Modal = styled.div`
  width: 500px;
  height: 550px;
  background-color: #2b2b2b;
  border-radius: 5px;
  @media ${deviceDF.mobileL} {
    width: 100%;
    height: auto;
  }
`;

export const ModalContent = styled.div`
  padding: 10px;
  align-items: center;
  font-family: "Conv_Adelle_Reg";
  color: white;
`;

export const Row = styled.div`
  width: 100%;
  height: auto; 
  display: flex;
  flex-wrap: wrap;
  padding: 60px;
  @media ${deviceDF.laptopS} {
    align-items: center;
    width: auto;
  }
  @media ${deviceDF.tabletM} {
    display: flex;
    flex-direction: column;
  }
`;
