const size = {
  mobileS: "375px",
  mobileM: "412px",
  mobileL: "580px",
  tabletS: "640px",
  tabletM: "768px",
  tabletL: "960px",
  laptopS: "1024px",
  laptopM: "1280px",
  laptopL: "1440px",
  desktop: "2560px",
};

export const deviceMF = {
  laptopS: `(min-width: ${size.laptopS})`,
};

export const deviceDF = {
  mobileS: `(max-width: ${size.mobileS})`,
  mobileM: `(max-width: ${size.mobileM})`,
  mobileL: `(max-width: ${size.mobileL})`,
  tabletS: `(max-width: ${size.tabletS})`,
  tabletM: `(max-width: ${size.tabletM})`,
  tabletL: `(max-width: ${size.tabletL})`,
  laptopS: `(max-width: ${size.laptopS})`,
  laptopM: `(max-width: ${size.laptopM})`,
  laptopL: `(max-width: ${size.laptopL})`,
  desktop: `(max-width: ${size.desktop})`,
};
