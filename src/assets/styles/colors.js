const yellowPrimary = "#f3cd06";
const yellowSecondary = "#e8bf13";
const black = "#222222";
const white = "#FFFFFF";
const blue = "#3352b5";

export { yellowPrimary, yellowSecondary, black, white, blue };
